var path = require('path')
var merge = require('webpack-merge')

module.exports = merge(require('./webpack.base.conf'), {
  context: __dirname,
  entry: '../src/dist.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'siop-ui.dist.js',
    library: 'siop-ui',
    libraryTarget: 'umd',
  },
  externals: {
    "vue": "Vue"
  },
  
})