import Vue from 'vue';
import Router from 'vue-router';
import SiopUI from '@/components/SiopUI';
import TableShowCase from '@/components/showcase/TableShowCase';
import CrudShowCase from '@/components/showcase/CrudShowCase';
import SelecaoListaDuplaShowCase from '@/components/showcase/SelecaoListaDuplaShowCase';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: SiopUI,
    },
    {
      path: '/table',
      name: 'table',
      component: TableShowCase,
    },
    {
      path: '/crud',
      name: 'crud',
      component: CrudShowCase,
    },
    {
      path: '/selecao-lista-dupla',
      name: 'selecao-lista-dupla',
      component: SelecaoListaDuplaShowCase,
    },
  ],
});
