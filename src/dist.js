import Paginador from './components/Paginador';
import Table from './components/Table';
import Crud from './components/Crud';

export default {
  install(Vue) {
    Vue.component('siop-paginador', Paginador);
    Vue.component('siop-table', Table);
    Vue.component('siop-crud', Crud);
  },
};

export { Paginador, Table, Crud };
